class
	GAMETEST


create
	make

feature
	g: GAME

	make
		do
			create g.make()
		end

	rollmany (n, pins: INTEGER)
		local
			i: INTEGER
		do
			from
				i := 0
			until
				i > n
			loop
				g.roll(pins)
			end
		end

	testGutterGame
		do
			rollmany(20,0)
		ensure
			g.score() = 0
		end

	testAllOnes
		do
			rollmany(20,1)
		ensure
			g.score() = 20
		end

	testOneSpare
		do
			g.roll(5)
			g.roll(5)
			g.roll(3)
			rollmany(17,0)
		ensure
			g.score() = 16
		end

	testOneStrike
		do
			g.roll(10)
			g.roll(3)
			g.roll(4)
			rollmany(16,0)
		ensure
			g.score() = 24
		end

	testPerfectGame
		do
			rollmany(12,10)
		ensure
			g.score() = 300
		end

	testLastSpare
		do
			rollmany(9,10)
			g.roll(5)
			g.roll(5)
			g.roll(10)
		ensure
			g.score() = 275
		end
end
