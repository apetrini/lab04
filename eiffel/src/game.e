class
    GAME

create
    make

feature

	rolls: ARRAY[INTEGER];
	currentRoll: INTEGER;
	capacity: INTEGER;

	make
		do
			currentRoll := 0;
			capacity := 23;
			create rolls.make(23,0);
		end

	roll (pins: INTEGER)
		do
			rolls.put(pins, currentRoll);
			currentRoll := currentRoll + 1;
		end

	score : INTEGER
		local
			totalScore: INTEGER
			frame: INTEGER
			i: INTEGER
		do
			totalScore := 0
			frame := 0
			i := 0

			from
				i := 0
			until
				i >= 10
			loop
				-- Strike
				if ((rolls @ frame) = 10) then
					totalScore := totalScore + 10 + (rolls @ (frame + 1)) + (rolls @ (frame + 2))
					frame := frame + 1
				-- Spare
				else if (((rolls @ frame) + (rolls @ frame + 1)) = 10) then
					totalScore := totalScore + 10 + (rolls @ frame + 2)
					frame := frame + 2
				-- no bonus
				else
					totalScore := totalScore + (rolls @ (frame + 1)) + (rolls @ (frame + 2))
					frame := frame + 2
				end

				i := i + 1
			end

			result := totalScore

		end
	end
end
